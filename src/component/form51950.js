import { Row, Col, Button, Input, Label, FormGroup } from "reactstrap"
function Form() {
    return (
        <div className="container">
            {/* tiêu đề */}
            <Row>
                <h3 className="text-center">Registraion form</h3>
            </Row>
            {/* nội dung */}
            <Row >
                <Row>
                    {/* Bên trái */}
                    <Col xs="6">

                        {/* firstName */}
                        <Row className="mt-2">
                            <Col xs="3">First Name <span className="text-danger"> (*)</span></Col>
                            <Col xs="9">
                                <input className="form-control"></input>
                            </Col>

                        </Row>
                        {/* lastNAme */}
                        <Row className="mt-2">
                            <Col xs="3">Last Name <span className="text-danger"> (*)</span></Col>
                            <Col xs="9">
                                <input className="form-control"></input>
                            </Col>

                        </Row>
                        {/* birthday */}
                        <Row className="mt-2">
                            <Col xs="3">Birth Day  <span className="text-danger"> (*)</span></Col>
                            <Col xs="9">
                                <input className="form-control"></input>
                            </Col>

                        </Row>
                        {/* gender */}
                        <Row className="mt-2">
                            <Col xs="3">Gender  <span className="text-danger"> (*)</span></Col>
                            <Col xs="9">
                                <Row>
                                    <Col xs="6">

                                        <FormGroup check>
                                            <Input
                                                name="radio1"
                                                type="radio"
                                            />
                                            {' '}
                                            <Label check>
                                                Male </Label>
                                        </FormGroup>
                                    </Col>
                                    <Col xs="6">
                                        <FormGroup check>
                                            <Input
                                                name="radio1"
                                                type="radio"
                                            />
                                            {' '}
                                            <Label check>
                                                Female </Label>
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                    {/* Bên phải */}
                    <Col xs="6">

                        {/* Passport */}
                        <Row className="mt-2">
                            <Col xs="3">Passport  <span className="text-danger"> (*)</span></Col>
                            <Col xs="9">
                                <input className="form-control"></input>
                            </Col>

                        </Row>
                        {/* Email */}
                        <Row className="mt-2">
                            <Col xs="3"> Email  </Col>
                            <Col xs="9">
                                <input className="form-control"></input>
                            </Col>

                        </Row>
                        {/* Country */}
                        <Row className="mt-2">
                            <Col xs="3"> Country  <span className="text-danger"> (*)</span></Col>
                            <Col xs="9">
                                <input className="form-control"></input>
                            </Col>
                        </Row>
                        {/* Region */}
                        <Row className="mt-2">
                            <Col xs="3"> Region  </Col>
                            <Col xs="9">
                                <input className="form-control"></input>
                            </Col>
                        </Row>

                    </Col>
                </Row>

            </Row>
            {/* nội dung 2*/}
            <Row>
                {/* Subject */}
                <Row className="mt-2">
                    <Col xs="1">Subject  <span className="text-danger"> (*)</span></Col>

                    <Col xs="11">
                        <input className="form-control"  style={{ "height": "100px" }}></input>
                    </Col>

                </Row>
            </Row>
            {/* nút */}
            <Row className="mt-5">
                <Col xs="8"></Col>
                <Col xs="4">
                    <Row>
                        <Col xs="6">
                            <Button color="success" style={{ "width": "150px" }}>check Data</Button>{" "}
                        </Col>
                        <Col xs="6">
                            <Button color="success" style={{ "width": "150px" }}>Send</Button>
                        </Col>
                    </Row>

                </Col>
            </Row>
        </div>
    )
}

export default Form